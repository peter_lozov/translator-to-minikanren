\documentclass[preprint,numbers,10pt]{sigplanconf}

\usepackage{makeidx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{indentfirst}
\usepackage{verbatim}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url}
%\usepackage[belowskip=-15pt,aboveskip=0pt]{caption}

\definecolor{light-gray}{gray}{0.90}
\newcommand{\graybox}[1]{\colorbox{light-gray}{\texttt{#1}}}

\lstdefinelanguage{ocaml}{
keywords={let, begin, end, in, match, type, and, fun, function, try, with, class, 
object, method, of, rec, repeat, until, while, not, do, done, as, val, inherit, 
new, module, sig, deriving, datatype, struct, if, then, else, ostap, open, generic, virtual},
sensitive=true,
basicstyle=\small,
commentstyle=\small\itshape\ttfamily,
keywordstyle=\ttfamily\underbar,
identifierstyle=\ttfamily,
basewidth={0.5em,0.5em},
columns=fixed,
fontadjust=true,
literate={->}{{$\to$}}1,
morecomment=[s]{(*}{*)}
}

\lstset{
basicstyle=\small,
identifierstyle=\ttfamily,
keywordstyle=\bfseries,
commentstyle=\scriptsize\rmfamily,
basewidth={0.5em,0.5em},
fontadjust=true,
escapechar=~,
language=ocaml
}

\sloppy
\newcommand{\cc}{\mbox{ :: }}
\newcommand{\opt}[1]{\mbox{[ }#1\mbox{ ] }}
\newcommand{\etc}{\mbox{ }\ldots}
\newcommand{\term}[1]{\mbox{\texttt{#1}}}
\newcommand{\nt}[1]{$#1$}
\newcommand{\meta}[1]{\textsc{#1}}
\newcommand{\spc}{\mbox{ }}
\newcommand{\alt}{\mbox{ | }}
\newcommand{\trans}[3]{{#1}\vdash{#2}\to{#3}}
\newcommand{\cd}[1]{\texttt{#1}}
\newcommand{\kw}[1]{\texttt{\textbf{#1}}}
\newcommand{\comm}[1]{\textit{\small{#1}}}
\newcommand{\tv}[1]{\mbox{$'{#1}$}}
\newcommand{\inmath}[1]{\mbox{\lstinline{#1}}}
\newcommand{\goal}{\mathfrak G}

\begin{document}

%\mainmatter

\title{Relational Conversion for Typed High-Order Functional Programs\\
{\small (Extended Abstract)}}

\authorinfo{Petr Lozov}
{St. Petersburg State University, St. Petersburg, Russia}
{lozov.peter@gmail.com\\
Postal address: 70/3, Botanicheskaya street, 198504, St. Petersburg, Russia\\
Scientific advisor: Dmitri Boulytchev\\
ACM student member number: 4970108\\
Category: graduate
}

%\author{}
%\institute{St. Petersburg State University, St. Petersburg, Russia\\
%\email{lozov.peter@gmail.com}\\
%Postal address: 70/3, Botanicheskaya street, 198504, St. Petersburg, Russia\\
%Scientific advisor: Dmitri Boulytchev\\
%ACM student member number: 4970108\\
%Category: graduate}

\maketitle

%\begin{multicols}{2}
\section{Introduction}

Relational programming~\cite{TRS} is an attractive technique, based on the idea 
of constructing programs as relations.  As a result, relational programs can be
``queried'' in various ``directions'', making it possible, for example, to simulate
reversed execution. Apart from being interesting from purely theoretical standpoint, 
this approach may have a practical value: some problems look much simpler, 
if considered as queries to some relational specification. There is a 
number of appealing examples, confirming this observation: a type checker 
for simply typed lambda calculus (and, at the same time, type inferencer and solver 
for the inhabitation problem), an interpreter (capable of generating ``quines''~--- 
programs, producing themselves as result)~\cite{Untagged}, list sorting (capable of 
producing all permutations), etc. 

Many logic programming languages, such as Prolog, Mercury\footnote{\url{https://mercurylang.org}}, 
or Curry\footnote{\url{http://www-ps.informatik.uni-kiel.de/currywiki}} are inherently relational
to some extent; there is, however, a language~--- miniKanren\footnote{\url{http://minikanren.org}}~--- 
which was specifically designed as a model for relational programming. Initially rather a 
minimalistic DSL for Scheme/Racket, which can be implemented in just a handful lines of code~\cite{MicroKanren}, 
miniKanren found its way in dozens of host languages, including Haskell, Standard ML, and OCaml~\cite{Ocanren}.

Writing relational programs directly sometimes can be tedious; however, in many cases 
the desirable description can be obtained from some functional program in a rather regular
way. We address the problem of converting high-order functional programs into relational form; our
solution generalizes the existing approach for the first-order case.

\section{Background and Related Works}

The problem of converting functional programs into relational form was addressed in preceding works on 
miniKanren~\cite{TRS,WillThesis}. According to the approach, proposed there, an $n$-argument function 
is converted into $n+1$ relation, where the $n+1$-th argument corresponds to the former 
function's result. This technique, however, can deal only with first-order programs. 

We argue, that in high-order case the conversion can not be performed for untyped terms, 
and develop an approach for typed case.

\section{Approach and Current Results}

Our approach is pretty much type-driven. We start from simply typed case (STLC~\cite{Barendregt} 
plus data constructors, pattern matching and primitive types) and enrich it with relational 
constructs and typing rules for them. 

Then we present a type conversion function $\left[\bullet\right]$, which for each type provides 
its relational counterpart. For example,

$$
\begin{array}{rcl}
\left[\inmath{int}\right]                   & = & \inmath{int} \to \goal \\
\left[\inmath{string}\to\inmath{int}\right] & = & (\inmath{string} \to \goal) \to (\inmath{int} \to \goal)
\end{array}
$$

\noindent where $\goal$ (\emph{goal type}) stands for the unique type for closed purely relational expressions. In other words,
we convert primitive values into unary relations and functions into high-order relations.

We present then structural conversion rules for terms, which project them into the purely relational
subset, and prove, that for a typed source program we always get a typed relational specification, thus
justifying the static soundness. There is some difficulty in justifying dynamic correctness, however, as
miniKanren currently does not have formally described semantics. We argue, that our approach provides 
extension for known first-order case, and demonstrate its feasibility in high-order case by the number
of examples, this justifying the correctness in an \emph{ad hoc} manner.

\section{Ongoing and Future Work}

We are working now on the extension of our approach for polymorphic case; it seems, that now not 
every program can be given a relational counterpart, so the first step is to restrict the type system to
type only the ``useful'' subset. After completing the ``pen-and-paper'' work we are going to implement the
convertor and apply it to obtain some useful relational specifications (the primary goals are, of course, 
some interpreters and miniKanren with constraints~\cite{CKanren}). Verifying the proofs in a proof assistant 
like Coq is also considered as an option.

Another direction of the research would be to develop a native type systems for relational programming.

\begin{thebibliography}{99}
\bibitem{TRS}
Daniel P. Friedman, William E.Byrd, Oleg Kiselyov. The Reasoned Schemer. The MIT
Press, 2005.

\bibitem{MicroKanren}
Jason Hemann, Daniel P. Friedman. $\mu$Kanren: A Minimal Core for Relational Programming //
Proceedings of the 2013 Workshop on Scheme and Functional Programming (Scheme '13).

\bibitem{Untagged}
William E. Byrd, Eric Holk, Daniel P. Friedman.
miniKanren, Live and Untagged: Quine Generation via Relational Interpreters (Programming Pearl) //
Proceedings of the 2012 Workshop on Scheme and Functional Programming (Scheme '12).

\bibitem{CKanren}
Claire E. Alvis, Jeremiah J. Willcock, Kyle M. Carter, William E. Byrd, Daniel P. Friedman.
cKanren: miniKanren with Constraints // 
Proceedings of the 2011 Workshop on Scheme and Functional Programming (Scheme '11).

\bibitem{WillThesis}
William E. Byrd. Relational Programming in miniKanren: Techniques, Applications, and Implementations.
Indiana University, Bloomington, IN, September 30, 2009.

\bibitem{Barendregt}
Henk Barendregt. Lambda Calculi with Types // Handbook of Logic in Computer Science, Volume II, Oxford University Press, 
1993.

\bibitem{Ocanren}
Dmitry Kosarev, Dmitry Boulytchev. Typed Embedding of a Relational Language in OCaml // ACM SIGPLAN Workshop on ML, 2016.

\end{thebibliography}
%\end{multicols}
\end{document}
