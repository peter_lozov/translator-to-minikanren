# README #

It's translator from subset of OCaml to OCanren.

## Subset of OCaml ##

Available sintax of OCaml:

```
S = | Variables 
    | Constants (false, 1, 2.0, '3', "4", etc.)
    | (=)
    | fun x -> S
    | S S
    | let f x1 ... xn = S in S
    | let rec f x1 ... xn = S in S
    | pattern matching
```

## Pattern matching ##

Pattern matching can be used in the following form:

```
match S with
| Cnstr_1 x_1 ... x_n1 -> S
| Cnstr_2 x_1 ... x_n2 -> S
              ...
| Cnstr_m x_1 ... x_nm -> S
```

Wild-card (_) is not available for use.

## Translating ##

```
translator [input] [output] -r
```

When using the parameter "-r" after the translation, a beta reduction will be performed.
