\documentclass [a4paper] {article}

% ----------------------------------------------------------------
% Required packages

\usepackage [T2A] {fontenc}
\usepackage [utf8] {inputenc}
\usepackage [english, russian] {babel}

\usepackage {url}
\usepackage [style = gost-numeric] {biblatex}

% ----------------------------------------------------------------
% Optional packages

\usepackage{lipsum}
\usepackage{amsmath, amssymb}
\usepackage{makeidx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{indentfirst}
\usepackage{verbatim}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{csquotes}

\lstdefinelanguage{ocaml}{
keywords={fresh, let, begin, end, in, match, type, and, fun, function, try, with, when, class,
object, method, of, rec, repeat, until, while, not, do, done, as, val, inherit,
new, module, sig, deriving, datatype, struct, if, then, else, open, private, virtual, include, @type},
sensitive=true,
commentstyle=\small\itshape\ttfamily,
keywordstyle=\ttfamily\underbar,
identifierstyle=\ttfamily,
basewidth={0.5em,0.5em},
columns=fixed,
fontadjust=true,
literate={->}{{$\to\;\;$}}3 {===}{{$\equiv$}}3 {=/=}{{$\not\equiv$}}3 {|>}{{$\triangleright$}}3 {|||}{{$\vee$}}3 {&&&}{{$\wedge$}}3 {fun}{{$\lambda$}}3 {!}{{$\uparrow$}}3,
morecomment=[s]{(*}{*)}
}

\lstset{
mathescape=true,
identifierstyle=\ttfamily,
keywordstyle=\bfseries,
commentstyle=\scriptsize\rmfamily,
basewidth={0.5em,0.5em},
fontadjust=true,
language=ocaml
}

\begin {document}

\section{Необходимость типизации}
Типизация необходима по нескольким причинам. Во-первых, для выявления программ, которые невозможно преобразовать в реляционную форму. Данный случай возможен при наличии в исходной функциональной программе конструктора с аргументом высшего порядка, так как для его обработки используется синтаксическая унификация. В качестве примера рассмотрим функцию $unbox$ (листинг~\ref{lozov-spbu:unbox-fun}).

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Функция unbox},label={lozov-spbu:unbox-fun},captionpos=b]
  let unbox = funx.
    match x with
    | Box y -> y
\end{lstlisting}

Данная функция принимает один аргумент и, если это возможно, снимает с него конструктор $Box$. При написании эквивалентного функции $unbox$ отношения (листинг~\ref{lozov-spbu:unbox-rel}) для снятия конструктора $Box$ необходимо использовать унификацию, которая применима только к аргументам первого порядка. Поэтому, например, если $z$ является функцией, то полученная программа некорректна и не может быть выполнена. Однако, при использовании, описанной нами, типизации программ, некорректные программы по-просту не типизируются.

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Функция unbox},label={lozov-spbu:unbox-rel},captionpos=b]
  let unbox = funx.funy.
    fresh (z)
      ((x === C z) &&&
      (y === x))
\end{lstlisting}

Также типизация необходима для преобразования конструкции сопоставления с образцом. Рассмотрим произвольное правило, используемое при сопоставлении. В общем случае оно имеет вид: 
$$C \; x_1 \ldots x_n \rightarrow B.$$
Для преобразования данного правила, с одной стороны, нужно описать непосредственно сопоставление (это производится с помощью унификации), с другой стороны, в случае успешного сопоставления необходимо исполнить выражение $B$. В случае, если $B$ --- это функция, необходимо информация о количестве её аргументов. Данная информация может быть получена, если исходная программа типизирована.

И, наконец, типизация нужна для оптимизации получаемого отношения, а именно для перехода от модели вычисления {\it call by name} к модели {\it call by value}.

\section{Реализация}
Для опробирования описанного выше преобразования был разработан транслятор из подмножества языка OCaml в OCanren.

Данный транслятор состоит из достаточно стандартных этапов: 
\begin{itemize}
\item построение абстрактного синтаксического дерева (АСТ);
\item вывод типов каждого узла АСТ;
\item преобразование функциональной программы в реляционную;
\item опциональная оптимизация - проведение бета-редукций;
\item генерация кода.
\end{itemize}

Для построения АСТ, вывода типов и генерации результирующего кода используются внутренние функции OCaml\footnote{\url{http://github.com/ocaml/ocaml}} (библиотека \texttt{compiler-libs}), а именно \texttt{Pparse.parse\_implementation}, \texttt{Typemod.type\_implementation} и \texttt{Pprintast.string\_of\_structure} соответственно.

Преобразование программы описано с помощью типа \texttt{Tast\_mapper.default}, в котором каждой конструкции языка OCaml соответствует функция преобразования. Данные функции были переопределены в соотвествии с правилами семантики. Фактически тип \texttt{Tast\_mapper.default} представляет из себя набор взаимнорекурсивных функций, которые совершают полный обход дерева программы. Таким образом преобразование функциональной программы было выполнено в виде ревурсивного спуска по структуре программы.

После преобразования программа содержит колоссальное количество абстракций, что вызывает заметное замедление как компиляции, так и исполнения результирующей программы в сравнении с исходной. Однако подавляющее число абстракций могут быть удалены с помощью проведения бета-редукций. Таким образом опциональная оптимизация, а именно проведение бета-редукций в результирующей программе, призвано ускорить как время компиляции этой программы, так и время её исполнения.

\section{Evaluation}

Были рассмотрены следующие программы: 
\begin{itemize}
\item интерпретатор высшего порядка для лямбда-исчисления, принимающий в качестве аргумента функцию поиска подвыражения, к которому необходимо применить бета-редукцию;
\item алгоритм синтаксической унификации;
\item алгоритм Хиндли-Милнера для вывода наиболее общего типа;
\item интерпретатор подмножества Scheme, реляционная форма которого позволяет генерировать квайны, трайны и т.д.
\end{itemize}

\subsection{Интерпретатор высшего порядка для лямбда-исчисления}

Отличительной особенностью данного интерпретатора является его аргумент высшего порядка, который используется для поиска подвыражения для проведения бета-редукции. Таким образом, в зависимости от этого аргумента можно получить интерпретатор с раличными стратегиями вычисления: {\it call by name}, {call by value}, нормальный порядок редуциии и др.

В качестве синтаксиса для лябда-исчисления была выбрана нотация Де Брюэна. Таким образом произвольный ламбда-терм может быть описан с помощью алгебраического типа $lambda$. Отметим, что в качестве параметра $'nat$ выступает алгебраический тип для чисел Пеано.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type ('nat, 'lambda) lambda = 
  | Index of 'nat
  | Abst of 'lambda
  | App of 'lambda * 'lambda
  
type ('lambda, 'lambda_with_hole) lambda_with_hole = 
  | Hole
  | AppL  of 'lambda_with_hole * 'lambda
  | AppR  of 'lambda * 'lambda_with_hole
  | AbstH of 'lambda_with_hole

type ('lambda, 'lambda_with_hole) beta = 
  | Nothing 
  | Beta of 'lambda_with_hole * 'lambda * 'lambda
\end{lstlisting}

Для представления данных о пригодном для бета-редукции подвыражении были описаны типы $lambda\_with\_hole$ и $beta$. Экземпляры типа $lambda\_with\_hole$ представляют собой ламбда-терм с пропуском. Данный терм содержит в своей структуре уникальное значение, замещающее удаленное подвыражение. Экземпляры типа $beta$ --- это либо кортежи из терма с пропуском, терма в который необходимо сделать подстановку и терма который необходимо подставить, либо пустое значение, использоемое в случае отсутствия в лямбда-терме подвыражений, которые необходимо редуцировать.

В конечном итоге реализованный функциональный интерпретатор имеет следующую сигнатуру.
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type lambda$_0$ = (nat$_0$, lambda$_0$) lambda
type beta$_0$ = (lambda$_0$, lambda_with_hole$_0$) beta
eval :: (lambda$_0$ -> beta$_0$) -> lambda$_0$ -> lambda$_0$.
\end{lstlisting}
Также были реализованы три стратегии вычисления: {\it call by name}, {call by value} и нормальный порядок редуции.

Полученное с помощью реляционного преобразования отношение позволяет:
\begin{itemize}
\item непосредственно интерпретировать лямбда-термы;
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 1 (eval normal_order 
                (=== (App(Abst(Index Z), Index (S Z)))) 
                q)
                
==> {
q=Index (S Z)
}
\end{lstlisting}
\item генерировать из нормальных форм термов множество термов, из которых эта нормальная форма была получена с помощью переданной отношения, определяющего стратегию вычисления.
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 5 (eval normal_order (=== q) (Index Z))
                
==> {
q=Index Z; 
q=App (Abst (Index Z), Index Z); 
q=App (Abst (Index (S Z)), _.18); 
q=App (Abst (Index Z), App (Abst (Index Z), Index Z)); 
q=App (Abst (Index Z), App (Abst (Index (S Z), _.45)); 
}

\end{lstlisting}
\end{itemize}
\subsection{Синтаксическая унификация}

Синтаксическая унификация по двум термам и некоторой подстановке переменных в термы позволяет минимально дополнить подстановку таким образом, чтобы эти два терма были равны.

В качестве множества доступных для унификации термов используется полиморфный тип $term$. Терм может быть либо переменной, либо конструктором от произвольного числа термов-аргументов. Типовые переменные отвечают за тип имен переменных и имен конструкторов.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type ('var, 'const, 'terms) term = 
 | Var of 'var
 | Cnst of 'const * 'terms
\end{lstlisting}

Для краткости в качестве типа $subst$ обозначим
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type ('a, 'b) term$_0$ = ('a, 'b, ('a, 'b) term$_0$ list 
type ('a, 'b) subst = ('a * ('a, 'b) term$_0$) list. 
\end{lstlisting}
Тогда написанная нами функция $unify$, имеет сингнатуру
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
unify :: ('a, 'b) term$_0$ -> ('a, 'b) term$_0$ -> 
                       ('a, 'b) subst -> ('a, 'b) subst option
\end{lstlisting}

Данная функция возвращает подстановку в случае её существования, либо $None$ в случае её отсутствия. В алгоритме используется {\it occurs check} для выявления вхождения переменной в соответствующий ей терм.

Реляционная форма позволяет:
\begin{itemize}
\item получить результирующую подстановку;
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 1 (unify (=== Var "x") (=== Cnst ("A", (=== []))) [] q)  
                
==> {
q=Some [("x", Cnst("A", []))]
}
\end{lstlisting}
\item генерировать один или оба терма, которые удовлетворяют итоговой подстановке;
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 1 (unify (=== q) 
                 (=== Cnst ("A", [])) 
                 (=== []) 
                 (Some [("z", Cnst ("A", []))]))  
                
==> {
q=Var "z"
}
\end{lstlisting}
\item подтвердить достижимость подстановки с помощью предъявления пары термов.
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q r) 1 (unify (=== q) 
                   (=== r) 
                   (=== []) 
                   (Some [("z", Cnst ("A", []))]))  
                
==> {
q=Var "x",
r=Cnst ("A", []); 

q=Cnst ("A", []), 
r=Var "x"; 

q=Cnst (_.32, [Var "x"]), 
r=Cnst (_.32, [Cnst ("A", [])]); 
}
\end{lstlisting}
\end{itemize}
\subsection{Вывод типов Хиндли-Милнера}

Данный алгоритм по лямбда-терму вычисляет наиболее общий тип этого терма.

Для описания используемой системы типов были написаны алгебраические типы $lambda\_type$ и $gen\_type$. Первый тип определяет две типовые контстанты $TInt$ и $TBool$, типовые именовынные переменные, а также объединение пары типов $a$ и $b$ в функциональный тип $a \rightarrow b$. Тип $gen\_type$ является полиморфным замыканием типа по списку имен типовых переменных.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type ('var, 'lambda_type) lambda_type =
 | TInt
 | TBool
 | TVar of 'var
 | TFun of 'lambda_type * 'lambda_type

type ('list, 'lambda_type) gen_type =
 | Gen of 'list * 'lambda_type
\end{lstlisting}

Синтаксис языка (типы $literal$ и $lambda$) состоит из констат, переменных, применения, абстакции и конструкции $let$. 

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type literal =
 | LInt of int
 | LBool of bool

type ('var, 'literal, 'lambda) lambda =
 | Var  of 'var
 | Lit  of 'literal
 | App  of 'lambda * 'lambda
 | Abst of 'var * 'lambda
 | Let  of 'var * 'lambda * 'lambda
\end{lstlisting}

Итоговая функция $type\_inference$ имеет следующий тип:
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type lambda_type$_0$ = (num, lambda_type$_0$) lambda_type
type lambda$_0$ = (list, literal, lambda$_0$) lambda

type_inference ::
  'a ->('a ->'a) ->'b lambda$_0$ ->'a lambda_type$_0$ option
\end{lstlisting}
Данная функция абстрагируется от типа имен типовых переменных с помощью первого и второго аргумента, а именно имени первой свободной типовой переменной, а также функции-генеретора следующей свободной переменной по предыдущей.

Например, если в качестве имен типовых переменных использовать числа Пеано, получим:
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
let nat_type_inference term = type_inference Z (fun n -> S n) term
\end{lstlisting}

Полученное с помощью реляционного преобразования отношение можо использовать для
\begin{itemize}
\item вычисления типа терма;
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 1 (nat_type_inference (=== Abst ("x", Var "x")) q)

==> {
q=Just (TFun (TVar Z, TVar Z)); 
}
\end{lstlisting}
\item проверки населенности типа с помощью генерации термов, которые имеют данный тип.
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 5 (nat_type_inference ((===) q) (Just TBool))

==> {}
q=Lit (LBool _.24); 
q=Let (_.18, Lit (LInt _.32), Lit (LBool _.80)); 
q=Let (_.18, Lit (LBool _.32), Lit (LBool _.80)); 
q=Let (_.89, Lit (LBool _.32), Var _.89); 
q=Let (_.18, Abst (_.26, Lit LInt (_.48)), Lit (LBool _.149)); 
}
\end{lstlisting}
\end{itemize}

Полученное отношение также абстрагировано от типа имен типовых переменных.

\subsection{Интерпретатор пожмножества языка Scheme}
Входным языком данного call-by-value интерпретатора является нетипизированное лямда-исчисление, дополненное конструктором списков и оператором цитирования. Оператор цитирования возвращает входной терм, не изменяя его.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type 'var ident = 
  | Lambda 
  | Quote 
  | List 
  | Var of 'var

type ('terms, 'ident) term = 
  | Ident of 'ident 
  | Seq of 'terms
\end{lstlisting}

Результатом выполнения входной программы является либо вычисленное значение, либо замыкание функции.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type ('term, 'ident, 'env) result = 
  | Val of 'term 
  | Closure of 'ident * 'term * 'env
\end{lstlisting}

Итоговый функциональный интерпретатор $eval$ имеет представленную ниже сингатуру.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
type term$_0$ = (term$_0$ list, string ident) term
type env = (string * term$_0$) list
type result$_0$ = (term$_0$, string ident, env) result

eval :: term$_0$ ->env ->result$_0$
\end{lstlisting}

Реляционная форма данного интерпретатора помимо интерпретирования входной программы позволяет находидить квайны произвольного порядка. Например, для вычисления квайна первого порядка достаточно запросить запросить программу, результатом вычисления которой в пустом окружении является она сама.

\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
run (q) 1 (eval (=== q) (===[]) (Val q))

==> {
q=Seq [
  Seq [Ident Lambda; Seq [Ident (_.253 [=/= List; =/= Quote])]; 
    Seq [Ident List; Ident (_.253 [=/= List; =/= Quote]); 
      Seq [Ident List; Seq [Ident Quote; Ident Quote];
        Ident (_.253 [=/= List; =/= Quote])]]];
    Seq [Ident Quote; 
      Seq [Ident Lambda; Seq [Ident (_.253 [=/= List; =/= Quote])]; 
        Seq [Ident List; Ident (_.253 [=/= List; =/= Quote]); 
          Seq [Ident List; Seq [Ident Quote; Ident Quote]; 
          Ident (_.253 [=/= List; =/= Quote])]]]]];
}
\end{lstlisting}
Если полученный результат представить в синтаксисе Scheme и подставить вместо семантической переменной $(\_.253)$ переменную $x$, получим
\begin{lstlisting}[language=ocaml,mathescape=true,numberstyle=\small,stepnumber=1,numbersep=-5pt]
((lambda(x) (list x (list (quote quote) x)))
 (lambda(x) (list x (list (quote quote) x))))
\end{lstlisting}

Полученная программа является квайном, что было проверено с помощью функциональной программы.

\section{Обозримый пример 1}

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Функция map},captionpos=b]
  let rec map = funf.funl.
    match l with
    | Nil          -> Nil
    | Cons (x, xs) -> Cons (f x, map f xs)
\end{lstlisting}

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Отношение map},captionpos=b]
  let rec map = funf.funl.funq.
    fresh (q$_1$) ((l q$_1$) &&&
      (((q$_1$ === Nil) &&& ((funq$_2$.q$_2$ === Nil) q)) |||
      (fresh (q$_3$ q$_4$)
        (q$_1$ === Cons (q$_3$, q$_4$)) &&&
        (funx.funxs.(funq$_5$.
          (fresh(q$_6$ q$_7$) 
            (f x q$_6$) &&&
            (map f xs q$_7$) &&&
            (q$_5$ === Cons(q$_6$, q$_7$)))) q
        ) ((===) q$_3$) ((===) q$_4$))))
\end{lstlisting}

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Отношение map после бета-редукций},captionpos=b]
  let rec map = funf.funl.funq.z
    fresh (q$_1$) ((l q$_1$) &&&
      (((q$_1$ === Nil) &&& (q === Nil)) |||
      (fresh (q$_3$ q$_4$)
        (q$_1$ === Cons (q$_3$, q$_4$))
          (fresh(q$_6$ q$_7$)
            (f ((===) q$_3$) q$_6$) &&&
            (map f ((===) q$_4$) q$_7$) &&&
            (q === Cons(q$_6$, q$_7$))))))
\end{lstlisting}

\section{Обозримый пример 2}
Здесь не описаны функции $index\_subst$ и $hole\_subst$.

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Функция eval},captionpos=b]
  let rec eval =funbeta_finder.funterm.
    match beta_finder term with
    | Beta (hole, body, arg) -> 
      let new_body = index_subst body Z arg in
      let new_term = hole_subst hole new_body in
      eval beta_finder new_term
    | Nothing -> term  
\end{lstlisting}

\begin{lstlisting}[language=ocaml,mathescape=true,numbers=left,numberstyle=\small,stepnumber=1,numbersep=-5pt,caption={Отношение eval после бета-редукций},captionpos=b]
  let rec eval=funbeta_finder.funterm.funq.
    fresh (q$_1$) ((term q$_1$) &&&
      (fresh (q$_2$ q$_3$ q$_4$) 
        (q$_1$ === Beta (q$_2$, q$_3$, q$_4$)) &&&
        (let new_body = index_subst ((===)q$_3$) ((===)Z) ((===)q$_4$) in
         let new_term = hole_subst ((===)q$_2$) new_body in
         eval funbeta_finder new_term)) |||
      (q$_1$ === Nothing) &&& (term q))
  
\end{lstlisting}
\end{document}