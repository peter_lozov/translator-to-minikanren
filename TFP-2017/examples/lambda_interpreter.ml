type 'nat a_nat = Z | S of 'nat

type compare =
  | Less
  | Equal
  | Greater

type ('nat, 'lambda) lambda = 
  | Index of 'nat
  | Abst of 'lambda
  | App of 'lambda * 'lambda

type ('lambda, 'lambda_with_hole) lambda_with_hole = 
  | Hole
  | AppL  of 'lambda_with_hole * 'lambda
  | AppR  of 'lambda * 'lambda_with_hole
  | AbstH of 'lambda_with_hole

type ('lambda, 'lambda_with_hole) beta = 
  | Nothing 
  | Beta of 'lambda_with_hole * 'lambda * 'lambda

(**************************************************************************)

let rec compare a b =
  match a with
  | Z ->
  begin
    match b with
    | Z    -> Equal
    | S b' -> Less
  end
  | S a' ->
    match b with
    | Z    -> Greater
    | S b' -> compare a' b'

(**************************************************************************)

let rec update_term term index =
  match term with
  | App (f, a) -> App (update_term f index, update_term a index)
  | Abst b     -> Abst (update_term b (S index))
  | Index i    ->
    match compare index i with
    | Equal   -> Index (S i)
    | Less    -> Index (S i)
    | Greater -> term 

(**************************************************************************)

let rec index_subst term index value =
  match term with
  | Abst body  -> Abst (index_subst body (S index) (update_term value Z))
  | App (f, a) -> App (index_subst f index value, index_subst a index value)
  | Index ind  -> 
    match compare index ind with
    | Equal   -> value
    | Greater -> term
    | Less    -> 
      match ind with 
      | S i -> Index i

(**************************************************************************)

let rec hole_subst hole_term value =
  match hole_term with
  | Hole -> value
  | AppL (ht, t) -> App (hole_subst ht value, t)
  | AppR (t, ht) -> App (t, hole_subst ht value)
  | AbstH b      -> Abst (hole_subst b value)

(**************************************************************************)

let rec eval beta_finder term =
  match beta_finder term with
  | Beta (hole, body, arg) -> 
    let new_body = index_subst body Z arg in
    let new_term = hole_subst hole new_body in
    eval beta_finder new_term
  | Nothing -> term               

(**************************************************************************)

let rec bind beta f =
  match beta with
  | Nothing                -> Nothing
  | Beta (hole, body, arg) -> Beta (f hole, body, arg)

(**************************************************************************)

let rec call_by_name term =
  match term with
  | Index i    -> Nothing
  | Abst b     -> Nothing
  | App (f, a) ->
    match f with
    | Abst b       -> Beta(Hole, b, a)
    | Index i      -> Nothing
    | App (f', a') -> bind (call_by_name f) (fun h -> AppL(h, a))

(**************************************************************************)

let rec call_by_value term =
  match term with
  | Index i    -> Nothing
  | Abst b     -> Nothing
  | App (f, a) ->
    let beta_a = bind (call_by_value a) (fun h -> AppR(f, h)) in
    match beta_a with
    | Beta (h, b, a) -> beta_a
    | Nothing ->
      match f with
      | Abst b       -> Beta(Hole, b, a)
      | Index i      -> Nothing
      | App (f', a') -> bind (call_by_value f) (fun h -> AppL(h, a))

(**************************************************************************)

let rec normal_order term =
  match term with
  | Index i    -> Nothing
  | Abst b     -> bind (normal_order b) (fun h -> AbstH h)
  | App (f, a) ->
    match f with
    | Abst b       -> Beta(Hole, b, a)
    | Index i      -> bind (normal_order a) (fun h -> AppR(f, h))
    | App (f', a') ->
      let beta_f = bind (normal_order f) (fun h -> AppL(h, a)) in
      match beta_f with
      | Beta (h, b, a) -> beta_f
      | Nothing        -> bind (normal_order a) (fun h -> AppR(f, h))

(**************************************************************************)

let id = Abst (Index Z)
let term = App(Abst(Abst(App(id, App(Index Z, Index (S Z))))), App(id, Index Z))


