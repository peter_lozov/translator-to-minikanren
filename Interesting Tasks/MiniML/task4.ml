type nat = O | S of nat

type step = Left  of nat 
          | Right of nat 
          | Fill  of nat 
          | Pour  of nat

type 'a maybe = Just of 'a
              | Nothing

let rec add a b =
  match a with
  | O   -> b
  | S x -> add x (S b)


let rec goe a b =
  match a with
  | O   -> b = O
  | S x ->
    match b with
    | O   -> true
    | S y -> goe x y


let rec sub a b =
  match b with
  | O   -> a
  | S y ->
    match a with
    | O   -> O
    | S x -> sub x y

let notO a =
  match a with
  | O   -> false
  | S b -> true


let not a =
  match a with
  | true  -> false
  | false -> true


let conj a b =
  match a with
  | true  -> b
  | false -> false


let disj a b =
  match a with
  | true  -> true
  | false -> b


let rec elem l n =
  match l with
  | x::xs ->
    match n with
    | O   -> x
    | S m -> elem xs m 


let rec changeElem l n f =
  match l with
  | x::xs ->
    match n with
    | O   -> f x :: xs
    | S m -> x   :: changeElem xs m f    


let checkStep step state len cop =
  match state with
  | (pos, fuel, sts) ->
    match step with
    | Left  d -> conj (conj (goe pos d)           (goe fuel d))    (notO d)
    | Right d -> conj (conj (goe len (add d pos)) (goe fuel d))    (notO d)
    | Pour  f -> conj (conj (not (pos = len))     (not (pos = O))) (notO f)
    | Fill  f -> conj (notO f) (
      match pos with
      | O   -> goe cop (add fuel f)
      | S x -> conj (goe (elem sts x) f) (goe cop (add fuel f)))


let step step state len =
  match state with
  | (pos, fuel, sts) ->
    match step with
    | Left  d -> (sub pos d, sub fuel d, sts)
    | Right d -> (add d pos, sub fuel d, sts)
    | Pour  f -> (
      match pos with
      | S x -> (pos, sub fuel f, changeElem sts x (fun e -> add f e)))
    | Fill  f ->
      match pos with
      | O   -> (pos, add f fuel, sts)
      | S x -> (pos, add f fuel, changeElem sts x (fun e -> sub e f))


let isFinishState st len =
  match st with
  | (pos, fuel, sts) -> pos = len


let updateTotalFuel step state prevFuel =
  match step with
  | Left d  -> prevFuel
  | Right d -> prevFuel
  | Pour f  -> prevFuel
  | Fill f  ->
    match state with
    | (pos, fuel, sts) ->
      match pos with
      | O   -> add f prevFuel
      | S x -> prevFuel


(*let printState st tf =
  let rec natToInt n =
    match n with
    | O   -> 0
    | S x -> 1 + natToInt x in  
  let rec list2str l =
    match l with
    | []    -> ""
    | x::xs -> Printf.sprintf "%d %s" (natToInt x) (list2str xs) in
  match st with
  | (p, f, s) -> Printf.printf "%d  %d  [ %s]  %d\n" (natToInt p) (natToInt f) (list2str s) (natToInt tf)*)
    

let checkAnswer answer len cop = 
  let rec check state ans totalFuel =
    (* printState state totalFuel; *)
    match ans with
    | [] -> (
      match isFinishState state len with
      | true  -> Just totalFuel
      | false -> Nothing)
    | x::xs ->
      match checkStep x state len cop with
      | false -> Nothing
      | true  -> 
        let newTF = updateTotalFuel x state totalFuel in
        check (step x state len) xs newTF in

  let startState =
    let rec stations n =
      match n with
      | O   -> []
      | S m -> O :: stations m in
    (O, O, stations len) in

  check startState answer O


(****************************************************************************)


let rec intToNat n =
  if n = 0 then O else S (intToNat (n-1))

let rec natToInt n =
  match n with
  | O   -> 0
  | S x -> 1 + natToInt x 

let one = intToNat 1
let two = intToNat 2
let fiv = intToNat 5
let sev = intToNat 7 

let answer = [Fill fiv; Right two; Pour one; Left two;
              Fill fiv; Right two; Pour one; Left two;
              Fill fiv; Right two; Fill two; Right fiv]


let print a l c =
  match checkAnswer a l c with
  | Nothing -> print_string "Baaaaad!\n"
  | Just n  -> Printf.printf "Ans: %d\n" (natToInt n)


let main = print answer sev fiv
