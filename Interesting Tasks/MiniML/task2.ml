type peano  = O | S of peano
type person = A | B | C | D
type step   = One of person | Two of person * person
type 'a opt = Nothing | Just of 'a


let rec greater a b =
  match a with
  | O   -> false
  | S x ->
    match b with
    | O  -> true
    | S y -> greater x y


let max a b =
  match greater a b with
  | true  -> a
  | false -> b


let rec add a b =
  match a with
  | O   -> b
  | S x -> add x (S b)


let conj a b =
  match a with
  | true  -> b
  | false -> false


let not a =
  match a with
  | true  -> false
  | false -> true


let checkPerson state person =
  match state with
  | (l, a, b, c, d) ->
    match person with
    | A -> a = l
    | B -> b = l
    | C -> c = l
    | D -> d = l


let checkStep state step =
  match step with
  | One p      -> checkPerson state p
  | Two (p, q) -> conj (checkPerson state p) (checkPerson state q)


let moveLight state =
  match state with
  | (l, a, b, c, d) -> (not l, a, b, c, d) 


let movePerson state person =
  match state with
  | (l, a, b, c, d) ->
    match person with
    | A -> (l, not a, b, c, d)
    | B -> (l, a, not b, c, d)
    | C -> (l, a, b, not c, d)
    | D -> (l, a, b, c, not d)


let step state step =
  match step with
  | One p      -> moveLight (movePerson state p)
  | Two (p, q) -> moveLight (movePerson (movePerson state p) q)

let getTime state times =
  match state with
  | One p      -> times p
  | Two (p, q) -> max (times p) (times q)


let getAnswer a times =
  let start  = (true, true, true, true, true) in
  let finish = (false, false, false, false, false) in

  let rec getAnswer a state num =
    match a with
    | [] -> (
      match state = finish with
      | true  -> Just num
      | false -> Nothing)
    | x::xs ->
      match checkStep state x with
      | false -> Nothing
      | true  ->
        let newNum   = add (getTime x times) num in
        let newState = step state x in
        getAnswer xs newState newNum in

  getAnswer a start O
        

let standartTimes p =
  match p with
  | A -> S O
  | B -> S (S O)
  | C -> S (S (S (S (S O))))
  | D -> S (S (S (S (S (S (S (S (S (S O)))))))))

(****************************************************************************)

let rec numToInt n =
  match n with
  | O   -> 0
  | S x -> 1 + numToInt x


let badAnswer  = [Two(A, B); One C]
let weakAnswer = [Two(A, B); Two(A, B); Two(A, B); One A; Two(A, C); One A; Two(A, D)]
let goodAnswer = [Two(A, B); One A; Two(A, C); One A; Two(A, D)]
let bestAnswer = [Two(A, B); One A; Two(C, D); One B; Two(A, B)]


let print a =
  match getAnswer a standartTimes with
  | Nothing -> print_string "Baaaaad!\n"
  | Just n  -> print_int (numToInt n);
               print_string "\n"


let main = print badAnswer;
           print weakAnswer;
           print goodAnswer;
           print bestAnswer
