type person = G | C | W | N


let not a = 
  match a with
  | true  -> false
  | false -> true


let checkState s =
  match s with
  | (i, g, c, w) ->
    match i = g with
    | true  -> true
    | false ->
      match i = c with
      | false -> false
      | true  -> i = w


let checkStep state step =
  match state with
  | (i, g, c, w) ->
    match step with
    | N -> true
    | G -> i = g
    | C -> i = c
    | W -> i = w


let step s p =
  match s with
  | (i, g, c, w) ->
    match p with
    | G -> (not i, not g, c, w)
    | C -> (not i, g, not c, w)
    | W -> (not i, g, c, not w)
    | N -> (not i, g, c, w)
    

let checkAnswer a =
  let startState  = (true, true, true, true) in
  let finishState = (false, false, false, false) in
  let rec checkAnswer a st =
    match a with
    | []    -> st = finishState
    | x::xs ->
      match checkStep st x with
      | false -> false
      | true  ->
        let newState = step st x in
          match checkState newState with
          | false -> false
          | true  -> checkAnswer xs newState in
   
  checkAnswer a startState

(****************************************************************************)

let print a =
  print_string (if checkAnswer a then "OK\n" else "Baaaad!\n")


let main = print [G; N; C; G; W; N; G];
           print [G; N; W; G; C; N; G]
