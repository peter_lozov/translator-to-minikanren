type bottle = Fst | Snd
type stepType = Fill | Empty | Pour
type nat = O | S of nat


let rec add a b =
  match a with
  | O   -> b
  | S x -> add x (S b)


let rec greater a b =
  match a with
  | O   -> false
  | S x ->
    match b with
    | O   -> true
    | S y -> greater x y


let rec sub a b =
  match b with
  | O   -> a
  | S y ->
    match a with
    | O   -> O
    | S x -> sub x y


let not b =
  match b with
  | true  -> false
  | false -> true


let disj a b =
  match a with
  | true  -> true
  | false -> b


let anotherBottle b =
  match b with
  | Fst -> Snd
  | Snd -> Fst


let createState bottle lvl1 lvl2 =
   match bottle with
   | Fst -> (lvl1, lvl2)
   | Snd -> (lvl2, lvl1)


let checkStep state step capacities =
  match state with
  | (f, s) ->
    match step with
    | (t, b) ->
      let lvl1 = match b with | Fst -> f | Snd -> s in
      let lvl2 = match b with | Fst -> s | Snd -> f in 
        match t with
        | Fill  -> not (lvl1 = capacities b) 
        | Empty -> not (lvl1 = O)
        | Pour  -> 
          let b'      = anotherBottle b in
          not (disj (lvl1 = O) (lvl2 = capacities b'))


let step state step capacities =
  match state with
  | (f, s) ->
    match step with
    | (t, b) ->
      let lvl2 = match b with | Fst -> s | Snd -> f in
        match t with
       | Fill  -> createState b (capacities b) lvl2
       | Empty -> createState b O lvl2
       | Pour  ->
         let sum  = add f s in
         let cap2 = capacities (anotherBottle b) in
         match greater sum cap2 with
         | true  -> createState b (sub sum cap2) cap2
         | false -> createState b O sum


let isFinishState state reqLvl =
  match state with
  | (f, s) -> disj (f = reqLvl) (s = reqLvl)


(*let printState st =
  let rec numToInt n =
    match n with
    | O   -> 0
    | S x -> 1 + numToInt x in

  match st with
  | (f, s) -> Printf.printf "%d, %d\n" (numToInt f) (numToInt s)
*)


let checkAnswer answer capacities reqLvl =
  let rec checkAnswer state answer = 
    (*printState state;*)
    match answer with
    | []      -> isFinishState state reqLvl
    | x :: xs ->
      match checkStep state x capacities with
      | false -> false
      | true  -> checkAnswer (step state x capacities) xs in

   let startState = (O, O) in
   checkAnswer startState answer


(****************************************************************************)


let capacities1 b =
  match b with
  | Fst -> S (S (S (S O)))
  | Snd -> S (S (S (S (S (S (S (S (S O))))))))


let reqLvl1 = S (S (S (S (S (S O)))))

let answer = [Fill, Fst;  Pour, Fst; Fill, Fst;  Pour, Fst; 
              Fill, Fst;  Pour, Fst; Empty, Snd; Pour, Fst;
              Fill, Fst;  Pour, Fst; Fill, Fst;  Pour, Fst;
              Empty, Snd; Pour, Fst; Fill, Fst;  Pour, Fst]

let badAns = (Empty, Fst) :: answer

let print a c r =
  print_string (if checkAnswer a c r then "OK!\n" else "Baaaad!\n")  

let main = 
  print answer capacities1 reqLvl1;
  print badAns capacities1 reqLvl1
