let () =
  let sourcefile    = Array.get Sys.argv 1 in
  let outputprefix  = Array.get Sys.argv 0 in
  let tast          = TastGetter.get_tast sourcefile outputprefix in
  Printtyped.implementation Format.std_formatter tast
  
