open Typedtree

;;(*****************************************************************************************************************************)


let get_tast sourcefile outputprefix =
  Clflags.dont_write_files := true;
  Clflags.recursive_types := true;
  Compmisc.init_path false; 
  
  let ppf          = Format.std_formatter in
  let modulename   = Compenv.module_of_filename ppf sourcefile outputprefix in 
  Env.set_unit_name modulename;

  let env       = Compmisc.initial_env() in
  let ast       = Pparse.parse_implementation ~tool_name:"testName" ppf sourcefile in
  let (tast, _) = Typemod.type_implementation sourcefile outputprefix modulename env ast in
  tast
 
