type ('a, 'b) llist = Nil | Cons of 'a * 'b

let rec compose f_gen l =
  let comp f g = fun x -> f (g x) in
  match l with
  | Nil         -> fun x -> x
  | Cons (h, t) -> comp (f_gen h) (compose f_gen t)
