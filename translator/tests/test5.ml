let test_consts x y =
   match x with
   | "true"  -> 1
   | "false" -> 2
   | "123"   -> 3
   | "fuf"   -> 
     match y with
     | true  -> 4
     | false -> 5

