type 'a l_list = E | C of 'a * 'a l_list

let rec append l s =
  match l with
  | E         -> s
  | C (a, ls) -> C (a, append ls s)
