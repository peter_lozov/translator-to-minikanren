type ('a, 'b) pair = P of 'a  * 'b

type 'a llist = Empty | Cons of 'a * 'a llist

type variable = First | Next of variable

type identifier = Lambda | Quote | List | Var of variable

type term = Ident of identifier | Seq of term llist

type result = Val of term | Closure of identifier * term * (identifier, result) pair llist

let identifier_equals x y =
  
  let rec variable_equals x y =
    match x with
    | First   -> ( match y with | First -> true  | Next y' -> false                 )
    | Next x' -> ( match y with | First -> false | Next y' -> variable_equals x' y' )

  in

  match x with
  | Lambda -> ( match y with | Lambda -> true  | Quote -> false | List -> false | Var y' -> false                 )
  | Quote  -> ( match y with | Lambda -> false | Quote -> true  | List -> false | Var y' -> false                 )
  | List   -> ( match y with | Lambda -> false | Quote -> false | List -> true  | Var y' -> false                 )
  | Var x' -> ( match y with | Lambda -> false | Quote -> false | List -> false | Var y' -> variable_equals x' y' )

let rec lookup x env =
  match env with
  | Cons (p, env') -> 
    match p with
    | P (y, res) ->
      match identifier_equals x y with
      | true  -> res
      | false -> lookup x env'

let rec not_in_env x env =
  match env with
  | Empty -> true
  | Cons (p, env') ->
    match p with
    | P (y, res) ->
      match identifier_equals x y with
      | true  -> false
      | false -> not_in_env x env'

let lambda_handler ts env = 
          match not_in_env Lambda env with 
          | true -> 
            match ts with
            | Cons (t1, ts1) -> 
              match t1 with
              | Seq l ->
                match l with
                | Cons (t2, ts2) ->
                  match t2 with
                  | Ident i ->
                    match ts2 with
                    | Empty ->
                      match ts1 with
                      | Cons (body, ts3) ->
                       match ts3 with
                       | Empty -> Closure (i, body, env)

let quote_handler ts env =
          match not_in_env Quote env with
          | true -> 
            match ts with
            | Cons (t1, ts1) ->
              match ts1 with
              | Empty -> Val t1

let rec map f l = match l with 
                  | Cons (x, xs) -> Cons (f x, map f xs) 
                  | Empty -> Empty

let rec eval term env =
  match term with
  | Ident x -> lookup x env
  | Seq l ->
    match l with
    | Cons (t, ts) ->
      match t with
      | Ident ident -> (
        match ident with
        | Lambda -> lambda_handler ts env
        | Quote -> quote_handler ts env
        | List -> (
          match not_in_env List env with
          | true ->
            let eval_val t = match eval t env with 
                             | Val v -> v in
            Val (Seq (map eval_val ts))))

      | Seq s ->
        match ts with
        | Cons (arg, ts1) ->
          match ts1 with
          | Empty ->
            match eval t env with
            | Closure (x, body, env') -> eval body (Cons (P (x, eval arg env), env'))
        


let x      = Ident (Var First)
let quote  = Ident Quote
let lambda = Ident Lambda
let lisT   = Ident List
let s x    = Seq x 

let l1 x   = Cons(x, Empty)
let ln x y = Cons(x, y)

let quine_func = s (ln lambda (ln (s (l1 x)) (l1 (s (ln lisT (ln x (l1 (s (ln lisT (ln (s (ln quote (l1 quote))) (l1 x)))))))))))
let quine_arg  = s (ln quote (l1 quine_func))

let quine = s (ln quine_func (l1 quine_arg))
