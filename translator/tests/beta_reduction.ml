let f1 a1 a2 a3 a4 a5 = (fun a b c d e -> a b c d e) a1 a2 a3 a4 a5 

let f2 a1 a2 a3 a4 a5 = (((((fun a b c d e -> a b c d e) a1) a2) a3) a4) a5 

let f3 a1 a2 a3 a4 a5 = (((fun a b c d e -> a b c d e) a1 a2) a3) a4 a5 

let f4 a1 a2 a3 a4 a5 = a1 a2 a3 a4 a5

let f5 a1 a2 a3 a4 a5 = (((a1 a2) a3) a4) a5

let f5 a1 a2 a3 a4 a5 = ((a1 a2) a3 a4) a5

let f6 a1 a2 a3 a4 a5 = (fun e -> (fun c d -> (fun a b -> a b c d e) a1) a3 a4) a5 a2

let f a1 a2 = (fun x y -> let r = x in r y) a1 a2 
