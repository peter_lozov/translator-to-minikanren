type 'a lambda = 
  | Var of 'a
  | Abst of 'a * 'a lambda
  | App of 'a lambda * 'a lambda

type 'a lambda_with_hole = 
  | Hole
  | AppL  of 'a lambda_with_hole * 'a lambda
  | AppR  of 'a lambda * 'a lambda_with_hole
  | AbstH of 'a * 'a lambda_with_hole

type 'a beta = 
  | Nothing 
  | Beta of 'a lambda_with_hole * 'a * 'a lambda * 'a lambda

(**************************************************************************)

let rec var_subst term var value =
  match term with
  | Var var' -> 
  begin
    match var = var' with
    | true  -> value
    | false -> term
  end
  | Abst(var', body) ->
  begin
    match var = var' with
    | true  -> term
    | false -> Abst (var', var_subst body var value)
  end
  | App (f, a) -> App (var_subst f var value, var_subst a var value)


(**************************************************************************)

let rec hole_subst hole_term value =
  match hole_term with
  | Hole -> value
  | AppL (ht, t) -> App (hole_subst ht value, t)
  | AppR (t, ht) -> App (t, hole_subst ht value)
  | AbstH (v, b) -> Abst (v, hole_subst b value)

(**************************************************************************)

let rec eval beta_finder term =
  match beta_finder term with
  | Beta (hole, var, body, arg) -> 
    let new_body = var_subst body var arg in
    let new_term = hole_subst hole new_body in
    eval beta_finder new_term
  | Nothing -> term               

(**************************************************************************)

let rec bind beta f =
  match beta with
  | Nothing                     -> Nothing
  | Beta (hole, var, body, arg) -> Beta (f hole, var, body, arg)

(**************************************************************************)

let rec call_by_name term =
  match term with
  | Var x       -> Nothing
  | Abst (v, b) -> Nothing
  | App (f, a)  ->
    match f with
    | Abst (v, b)  -> Beta(Hole, v, b, a)
    | Var x        -> Nothing
    | App (f', a') -> bind (call_by_name f) (fun h -> AppL(h, a))

(**************************************************************************)

let rec call_by_value term =
  match term with
  | Var x       -> Nothing
  | Abst (v, b) -> Nothing
  | App (f, a)  ->
    let beta_a = bind (call_by_value a) (fun h -> AppR(f, h)) in
    match beta_a with
    | Beta (h, v, b, a) -> beta_a
    | Nothing ->
      match f with
      | Abst (v, b)  -> Beta(Hole, v, b, a)
      | Var x        -> Nothing
      | App (f', a') -> bind (call_by_value f) (fun h -> AppL(h, a))

(**************************************************************************)

let rec normal_order term =
  match term with
  | Var x       -> Nothing
  | Abst (v, b) -> bind (normal_order b) (fun h -> AbstH(v, h))
  | App (f, a)  ->
    match f with
    | Abst (v, b)  -> Beta(Hole, v, b, a)
    | Var x        -> bind (normal_order a) (fun h -> AppR(f, h))
    | App (f', a') ->
      let beta_f = bind (normal_order f) (fun h -> AppL(h, a)) in
      match beta_f with
      | Beta (h, v, b, a) -> beta_f
      | Nothing           -> bind (normal_order a) (fun h -> AppR(f, h))

(**************************************************************************)

let id = Abst("x", Var "x")
let term = App(Abst("y", Abst("z", App(id, App(Var "z", Var "y")))), App(id, Var "a"))


