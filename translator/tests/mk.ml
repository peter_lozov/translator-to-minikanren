type 'a my_list = 
 | Nil 
 | Cons of 'a * 'a my_list

type ('a, 'b) pair =
 | Pair of 'a * 'b

type num =
 | Z
 | S of num

type syn_term = 
 | Var of string
 | Cnst of string * syn_term my_list

type sem_term =
 | SVar of num
 | SCnst of string * sem_term my_list

type 'a maybe = 
 | Nothing
 | Just of 'a

type goal =
 | EQC    of syn_term * syn_term
 | DEC    of syn_term * syn_term
 | Fresh  of string * goal
 | Disj   of goal * goal
 | Conj   of goal * goal
 | LetRec of string * string * goal * goal
 | App    of string * syn_term

type value =
 | Term of sem_term
 | Rec  of string * goal * (string, value) pair my_list

type stream =
 | Empty
 | List of (num, sem_term) pair my_list * (num, sem_term) pair my_list my_list * (string, value) pair my_list * num * stream
 | Mplus of stream * stream 
 | Bind  of stream * goal

(*********************************************************************************)

let rec map f l =
  match l with
  | Nil          -> Nil
  | Cons (x, xs) -> Cons (f x, map f xs)

(*********************************************************************************)

let rec filter pred l =
  match l with
  | Nil          -> Nil
  | Cons (x, xs) ->
    match pred x with
    | true  -> Cons (x, filter pred xs) 
    | false -> filter pred xs

(*********************************************************************************)

let rec foldl f acc l =
  match l with
  | Nil          -> acc
  | Cons (x, xs) -> foldl f (f acc x) xs

(*********************************************************************************)

let rec zip l1 l2 =
  match l1 with
  | Nil            -> Nil
  | Cons (x1, xs1) ->
    match l2 with
    | Nil            -> Nil
    | Cons (x2, xs2) -> Cons (Pair (x1, x2), zip xs1 xs2)

(*********************************************************************************)

let rec append x y =
  match x with
  | Nil           -> y
  | Cons (hd, tl) -> Cons (hd, append tl y)

(*********************************************************************************)

let bool_or x y =
  match x with
  | false -> y
  | true  -> true

let not x =
  match x with
  | true -> false
  | false -> true

(*********************************************************************************)

let comp f g x = f (g x)

(*********************************************************************************)
(*********************************************************************************)
(*********************************************************************************)

let rec lookup v s =
  match s with
  | Nil -> Nothing
  | Cons (pair, xs) ->
    match pair with
    | Pair (v', t) ->
      match v = v' with
      | true  -> Just t
      | false -> lookup v xs

(*********************************************************************************)

let rec walk t s =
  match t with
  | SCnst (n, l) -> t
  | SVar v       ->
    match lookup v s with
    | Nothing -> t
    | Just t' -> walk t' s

(*********************************************************************************)

let rec occurs_check x t s =
  match walk t s with
  | SVar v       -> x = v
  | SCnst (n, l) -> foldl (fun acc p -> bool_or acc (occurs_check x p s)) false l

(*********************************************************************************)

let extS x v s delta = 
  match occurs_check x v s with
  | true  -> Nothing
  | false -> 
    let diff  = Pair (x, v) in
    let new_s = Cons (diff, s) in
    let new_d = Cons (diff, delta) in
    Just (Pair (Just new_s, new_d))

(*********************************************************************************)

let unify x y s =

  let rec unify x y s delta =

    let ret s d = Just (Pair (s, d)) in

    let rec unify_lists l1 l2 s delta =
      match s with
      | Nothing -> ret Nothing delta
      | Just s'  ->
        match l1 with
        | Nil ->
        begin
          match l2 with
          | Nil          -> ret s delta
          | Cons (y, ys) -> ret Nothing delta
        end
        | Cons (x, xs) ->
          match l2 with
          | Nil          -> ret Nothing delta
          | Cons (y, ys) -> 
            match unify x y s' delta with
            | Nothing -> Nothing
            | Just p  ->
              match p with
              | Pair (new_s, new_d) -> 
                unify_lists xs ys new_s new_d in

    let wx = walk x s in
    let wy = walk y s in

    match wx with
    | SVar v -> 
    begin
      match wy with
      | SCnst (n, l) -> extS v wy s delta
      | SVar v'      ->
        match v = v' with
        | true  -> ret (Just s) delta
        | false -> extS v wy s delta
    end
    | SCnst (n, l) ->
      match wy with
      | SVar v         -> extS v wx s delta
      | SCnst (n', l') ->
        match n = n' with
        | false -> ret Nothing delta
        | true  -> unify_lists l l' (Just s) delta in

  unify x y s Nil

(*********************************************************************************)

let equality a b s constrs env num =

  let check_constr_elem acc constr_elem =
    match acc with
    | Nothing   -> acc
    | Just pair ->
      match pair with
      | Pair (subst, acc') ->
        match constr_elem with
        | Pair (var, term) ->
          match unify (SVar var) term subst with
          | Nothing -> Nothing
          | Just p ->
            match p with
            | Pair (s, d) ->
             match s with
             | Nothing -> Nothing
             | Just s' -> Just (Pair (s', append d acc')) in

  let check_const subst acc constr =
    match acc with
    | Nothing   -> acc
    | Just acc' ->
      match foldl check_constr_elem (Just (Pair (subst, Nil))) constr with
      | Nothing   -> acc
      | Just pair ->
        match pair with
        | Pair (s, d) ->
         match d with
          | Nil          -> Nothing
          | Cons (x, xs) -> Just (Cons (d, acc')) in
        
  match unify a b s with
  | Nothing   -> Empty
  | Just pair ->
    match pair with
    | Pair (s', d) ->
      match s' with
      | Nothing  -> Empty
      | Just s'' ->
        match foldl (check_const s'') (Just Nil) constrs with
        | Nothing          -> Empty
        | Just new_constrs -> List (s'', new_constrs, env, num, Empty)
          
(*********************************************************************************)

let desequality a b s constrs env num =
  match unify a b s with
  | Nothing -> List(s, constrs, env, num, Empty)
  | Just p  ->
    match p with
    | Pair (s', d) ->
      match s' with
      | Nothing  -> List(s, constrs, env, num, Empty)
      | Just s'' ->
        match d with
        | Nil          -> Empty
        | Cons (x, xs) -> List (s, Cons(d, constrs), env, num, Empty)

(*********************************************************************************)
(*********************************************************************************)
(*********************************************************************************)

let rec eval_term env t =

  let rec map_args acc l =
    match l with
    | Nil          -> acc Nil
    | Cons (x, xs) ->
      match eval_term env x with
      | Nothing -> Nothing
      | Just x' -> map_args (fun a -> acc (Cons(x', a))) xs in

  match t with
  | Cnst (c, l) ->
  begin
    match map_args (fun a -> Just a) l with
    | Nothing -> Nothing
    | Just l' -> Just (SCnst (c, l'))
  end
  | Var v ->
    match lookup v env with
    | Nothing    -> Nothing
    | Just value ->
      match value with
      | Term t'         -> Just t'
      | Rec (n, g, env) -> Nothing
 
(*********************************************************************************)

let rec eval g env num s c =
  match g with
  | EQC (t1, t2) -> 
  begin
    match eval_term env t1 with
    | Nothing  -> Nothing
    | Just t1' ->
      match eval_term env t2 with
      | Nothing  -> Nothing
      | Just t2' -> Just (Pair (equality t1' t2' s c env num, num))
  end
  | DEC (t1, t2) ->
  begin
    match eval_term env t1 with
    | Nothing  -> Nothing
    | Just t1' ->
      match eval_term env t2 with
      | Nothing  -> Nothing
      | Just t2' -> Just (Pair (desequality t1' t2' s c env num, num))
  end
  | App (var, term) ->
  begin
    match lookup var env with
    | Nothing    -> Nothing
    | Just value ->
      match value with
      | Term t            -> Nothing
      | Rec (v, g', env') ->
        match eval_term env term with
        | Nothing -> Nothing
        | Just t' -> eval g' (Cons (Pair (var, value), (Cons (Pair (v, Term t'), env')))) num s c
  end
  | LetRec (n, v, b, g) -> eval g (Cons (Pair (n, Rec (v, b, env)), env)) num s c
  | Fresh (var, g)      -> eval g (Cons (Pair (var, Term (SVar num)), env)) (S num) s c
  | Disj (g1, g2)       -> 
  begin
    match eval g1 env num s c with
    | Nothing  -> Nothing
    | Just p1 ->
      match p1 with
      | Pair (st1, n1) ->
        match eval g2 env n1 s c with
        | Nothing -> Nothing
        | Just p2 ->
          match p2 with
          | Pair (st2, n2) -> Just (Pair (Mplus (st1, st2), n2))
  end
  | Conj (g1, g2) -> 
    match eval g1 env num s c with
    | Nothing -> Nothing
    | Just p  ->
      match p with
      | Pair (st, n) -> Just (Pair (Bind (st, g2), n))

(*********************************************************************************)

let rec mplus st1 st2 =
  match st1 with
  | Empty                -> st2
  | List (s, c, e, n, t) -> List (s, c, e, n, Mplus(st2, t))
  | Mplus (st1', st2')   -> Mplus (st2, mplus st1' st2')
  | Bind (st, g)         -> Mplus (st2, bind st g)

and bind st g =
  match st with
  | Empty                -> Empty
  | Mplus (st1', st2')   -> Bind (mplus st1' st2', g)
  | Bind (st', g')       -> Bind (bind st' g', g)
  | List (s, c, e, n, t) -> 
    match eval g e n s c with
    | Nothing -> Empty
    | Just p  ->
      match p with
      | Pair (st0, n') -> Mplus (st0, Bind (t, g))

(*********************************************************************************)

let rec take n st =
  match n with
  | Z    -> Nil
  | S n' ->
    match st with
    | Empty                -> Nil
    | List (s, c, e, n, t) -> Cons (Pair (s, c), take n' t)
    | Mplus (st1, st2)     -> take n (mplus st1 st2)
    | Bind (st, g)         -> take n (bind st g)

(*********************************************************************************)

let run_goal n g =
  match eval g Nil Z Nil Nil with
  | Nothing -> Nothing
  | Just p  ->
    match p with
    | Pair (st, n') -> Just (take n st)

(*********************************************************************************)
(*********************************************************************************)
(*********************************************************************************)

let app = App ("dog", Var "x")

let dog hole = 
  Fresh ("x", Fresh ("y",
    LetRec ("dog", "a",
      Disj (
        EQC (Var "a", Cnst ("Dog", Nil)),
        Fresh ("d",
          Conj (
            EQC (Var "a", Cnst ("Hot", Cons (Var "d", Nil))),
            App ("dog", Var "d")
          )
        )
      ),
      Conj (
        DEC (Var "x", Cnst ("Hot", Cons (Var "y", Nil))),
        hole
      )
    )
  ))

let real_dog = dog app

let add_subst = Cnst ("S", Cons (Var "c0", Nil))

let add hole = 
  Fresh ("q", Fresh ("r", Fresh ("arg",
    LetRec ("add", "x", 
      Fresh ("a", Fresh ("b", Fresh ("c", 
        Conj (
          EQC (Var "x", Cnst ("T", Cons (Var "a", Cons (Var "b", Cons (Var "c", Nil))))),
          Disj (
            Conj (
              EQC (Var "a", Cnst ("Z", Nil)),
              EQC (Var "b", Var "c")
            ),
            Fresh ("a0", Fresh ("c0", Fresh ("x0",
              Conj (
                EQC(Var "a", Cnst ("S", Cons (Var "a0", Nil))),
                Conj (
                  EQC (Var "c", hole),
                  Conj (
                    EQC (Var "x0", Cnst ("T", Cons (Var "a0", Cons (Var "b", Cons (Var "c0", Nil))))),
                    App ("add", Var "x0")
                  )
                )
              )
            )))
          )
        )
      ))),
      Conj (
        EQC (Var "arg", Cnst ("T", Cons (Var "q", Cons (Var "r", Cons (Cnst ("S", Cons (Cnst ("S", Cons (Cnst ("Z", Nil), Nil)), Nil)), Nil))))),
        App ("add", Var "arg")
      )
    )
  )))

let real_add = add add_subst

let num = (S (S (S (S Z))))

let answer = run_goal num real_add

(*********************************************************************************)
(*************************** Support functions ***********************************)
(*********************************************************************************)

(*
let print_num num =
  let rec print_num num acc =
    match num with
    | Z   -> acc
    | S n -> print_num n (acc + 1) in
  print_num num 0 |> string_of_int
 
let rec print_term t =
  match t with
  | SVar v       -> "_" ^ print_num v
  | SCnst (c, l) -> 
    match l with
    | Nil          -> c
    | Cons (x, xs) -> c ^ " (" ^ print_term x ^ foldl (fun acc x' -> ", " ^ print_term x' ^ acc) ")" xs

let print_pair p =
  match p with
  | Pair (var, term) -> "_" ^ print_num var ^ " = " ^ print_term term

let print_subst pref s =
  foldl (fun acc p -> pref ^ print_pair p ^ "\n" ^ acc) "" s


let rec print_res res =
  match res with
  | Nil -> "Empty\n"
  | Cons (x, xs) ->
    match x with
    | Pair(s, c) -> "Subst: {\n" ^ print_subst "  " s ^ "}\nConstrainsts: {\n" ^ 
                    foldl (fun acc c' -> "  Constranint: {\n" ^ print_subst "    " c' ^ "  }" ^ acc) "" c ^ "}" ^
                    "\n\n" ^ print_res xs

let print_r res =
  res |> print_res |> print_string;
  print_string "\n\n"

let print_unify_res res =
  match res with
  | Nothing -> "OC"
  | Just p ->
    match p with
    | Pair (s, d) ->
      match s with
      | Nothing -> "Subst:\n  none\nDelta:\n" ^ print_subst "  " d
      | Just s  -> "Subst:\n" ^ print_subst "  " s ^ "Delta:\n" ^ print_subst "  " d 

(*********************************************************************************)
(*********************************************************************************)
(*********************************************************************************)


let _ =
  let res = run_goal (S (S (S (S Z)))) (real_add) in
  match res with
  | Just r -> print_r r
  | Nothing -> print_string "Error\n"

let _ =
  let res = run_goal (S (S (S (S Z)))) (add add_subst) in
  match res with
  | Just r -> print_r r
  | Nothing -> print_string "Error\n"
  
*)
