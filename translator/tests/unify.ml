type 'a my_list = 
 | Nil 
 | Cons of 'a * 'a my_list

type ('a, 'b) pair =
 | Pair of 'a * 'b

type ('var, 'const) term = 
 | Var of 'var
 | Cnst of 'const * ('var, 'const) term my_list

type 'a maybe = 
 | Nothing
 | Just of 'a

(*********************************************************************************)

let rec foldl f acc l =
  match l with
  | Nil          -> acc
  | Cons (x, xs) -> foldl f (f acc x) xs

(*********************************************************************************)

let bool_or x y =
  match x with
  | false -> y
  | true  -> true

(*********************************************************************************)
(*********************************************************************************)
(*********************************************************************************)

let rec lookup v s =
  match s with
  | Nil -> Nothing
  | Cons (pair, xs) ->
    match pair with
    | Pair (v', t) ->
      match v = v' with
      | true  -> Just t
      | false -> lookup v xs

(*********************************************************************************)

let rec walk t s =
  match t with
  | Cnst (n, l) -> t
  | Var v       ->
    match lookup v s with
    | Nothing -> t
    | Just t' -> walk t' s

(*********************************************************************************)

let rec occurs_check x t s =
  match walk t s with
  | Var v       -> x = v
  | Cnst (n, l) -> foldl (fun acc p -> bool_or acc (occurs_check x p s)) false l

(*********************************************************************************)

let extS x v s = 
  match occurs_check x v s with
  | true  -> Nothing
  | false -> Just (Cons (Pair (x, v), s))

(*********************************************************************************)

let rec unify x y s =

  let rec unify_lists l1 l2 s =
    match s with
    | Nothing -> Nothing
    | Just s'  ->
      match l1 with
      | Nil ->
      begin
        match l2 with
        | Nil          -> s
        | Cons (y, ys) -> Nothing
      end
      | Cons (x, xs) ->
        match l2 with
        | Nil          -> Nothing
        | Cons (y, ys) -> unify_lists xs ys (unify x y s') in

  let wx = walk x s in
  let wy = walk y s in

  match wx with
  | Var v -> 
  begin
    match wy with
    | Cnst (n, l) -> extS v wy s
    | Var v'      ->
      match v = v' with
      | true  -> Just s
      | false -> extS v wy s
  end
  | Cnst (n, l) ->
    match wy with
    | Var v         -> extS v wx s
    | Cnst (n', l') ->
      match n = n' with
      | false -> Nothing
      | true  -> unify_lists l l' (Just s)

(*********************************************************************************)

let s = unify (Var 'x') (Cnst ("A", Cons (Var 'x', Nil))) Nil
let a = Nil
let b = Just (Cons(Pair('x', Cnst("C", Nil)), Cons(Pair('y', Var 'z'), Nil)))
