open GT
open MiniKanren
open Tester

@type term =
  | Var of string logic
  | LInt of int logic
  | LBool of bool logic
  | App of term logic * term logic 
  | Abst of string logic * term logic 
  | Let of string logic * term logic * term logic with show

@type 'a typ =
  | TInt
  | TBool
  | TVar of 'a
  | TFun of 'a typ logic * 'a typ logic with show

@type 'a llist =
  | Nil
  | Cons of 'a * 'a llist logic with show

@type ('a, 'b) pair =
  | Pair of 'a * 'b with show

@type 'a gen_typ =
  | Type of 'a typ logic
  | Poly of term logic * (string logic, 'a gen_typ logic) pair logic llist logic with show


let rec interference e env t = 
  conde [
    fresh (v)
      (e === !!(Var v))
      (lookup v env t);
    fresh (i)
      (e === !!(LInt i))
      (t === !!TInt);
    fresh (b)
      (e === !!(LBool b))
      (t === !!TBool);
    fresh (v b t1 t2)
      (e === !!(Abst (v, b)))
      (t === !!(TFun (t1, t2)))
      (interference b (!!(Cons (!! (Pair (v, !!(Type t1))), env))) t2);
    fresh (e1 e2 t')
      (e === !!(App (e1, e2)))
      (interference e1 env (!!(TFun (t', t))))
      (interference e2 env t');
    fresh (v e1 e2 t_ignore)
      (e === !!(Let (v, e1, e2)))
      (interference e1 env t_ignore)
      (interference e2 (!!(Cons (!! (Pair (v, !!(Poly (e1, env)))), env))) t)
  ]

and lookup v env t =
  conde [
    fresh (exp env' tail)
      (env === !! (Cons (!! (Pair (v, !! (Poly (exp, env')))), tail)))
      (interference exp env' t);
    fresh (tail)
      (env === !!(Cons (!! (Pair (v, !! (Type t))), tail)));
    fresh (v' t' tail)
      (env === !!(Cons (!! (Pair (v', t')), tail)))
      (v =/= v')
      (lookup v tail t)
  ]

(****************************************)

let llet v e1 e2 = !!(Let (!!v, e1, e2))
let app e1 e2 = !!(App (e1, e2))
let abst v b = !!(Abst (!!v, b))
let var v = !! (Var (!! v))

let tFun (t1, t2) = !!(TFun (t1, t2))
let tVar v = !!(TVar v)

let id = abst "x" (var "x")
let t1 part = llet "f" part (app (var "f") (abst "y" (app (var "f") (var "y"))))

let f = abst "x" (abst "y" (app (var "y") (var "x")))
let t2 part = llet "f" part (app (var "f") (app (var "f") (var "f")))


let type_show = show logic (show typ (show logic (show int)))
let term_show = show logic (show term)

let _ =
 run type_show 1 q (REPR (fun q -> interference (!!(LInt (!!5))) (!!Nil) q )) qh;

 run type_show 1 q (REPR (fun q -> interference (t1 id) (!!Nil) q )) qh;
 run term_show 5 q (REPR (fun q -> 
                            fresh (x)
                              (interference 
                                   (t1 q) 
                                   (!!Nil) 
                                   (!!(TFun (!! (TVar x), !! (TVar x))))
                              ) 
                         )) qh;

  run type_show 1 q (REPR (fun q -> interference (t2 f) (!!Nil) q )) qh;
  run term_show 5 q (REPR (fun q -> 
                            fresh (x y z a)
                              (interference 
                                   (t2 q) 
                                   (!!Nil) 
                                   (tFun (tFun (tFun (tFun (tFun (tVar x, tFun (tFun (tVar x, tVar y), tVar y)), tVar z), tVar z), tVar a), tVar a))
                              ) 
                         )) qh;

