open GT
open MiniKanren
open Tester

@type 'a my_list =
  | Nil 
  | Cons of 'a* 'a my_list logic  with show
@type num =
  | Z 
  | S of num logic  with show
@type lambda =
  | Index of num logic 
  | Abst of lambda logic 
  | App of lambda logic* lambda logic  with show
@type res =
  | Value of num logic 
  | Closure of lambda logic* res logic my_list logic with show

(****************************************************************)

let rec get arr i res =
  fresh (h t j)
    (arr === (!!(Cons (h, t))))
    (
      ((i === (!!Z)) &&& (res === h))
      |||
      ((i === (!!(S j))) &&& (get t j res))
    )

(****************************************************************)

let rec eval term env res = (
  fresh (i) 
    (term === (!!(Index i))) 
    (get env i res)
  ) ||| (
  fresh (b) 
    (term === (!!(Abst b))) 
    (res === (!!(Closure(term, env))))
  ) ||| (
  fresh (f a res_f f' env' res_a) 
    (term === (!!(App (f, a))))
    (eval f env res_f)
    (res_f === (!!(Closure (f', env'))))
    (eval a env res_a)
    (eval f' (!! (Cons (res_a, env'))) res)
  )

(****************************************************************)

let z = !!Z
let s i = !!(Z i)
let index i = !!(Index i)
let abst b = !!(Abst b)
let app f a = !!(App (f, a))

let term1 = app (abst (index z)) (abst (index z))
let term2 = app (abst (abst (index z))) (abst (index z))

(****************************************************************)

let show_res       = show logic (show res)

let _ =
  run show_res        1    q (REPR (fun q     -> eval term1 (!!Nil) q))   qh;
  run show_res        1    q (REPR (fun q     -> eval term2 (!!Nil) q))   qh;
